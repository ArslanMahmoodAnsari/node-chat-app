const expect = require("expect");
const {Users} = require('./users');

describe('Users', () => {
    let tempUsers;

    beforeEach(()=>{
    tempUsers = new Users();
    tempUsers.users = [{
        id:'1',      
        name:'user 1',    
        room:'room 1'     
        },{
        id:'2',      
        name:'user 2',    
        room:'room 1'     
        },{
        id:'3',      
        name:'user 3',    
        room:'room 2'     
    }]    
    })

    it('Should add a new user', () => {
        let users = new Users();
        let user ={
        }
        let resUser = users.addUser(user.id, user.name, user.room);
        expect(users.users).toEqual([user]);
    });

    it('should return names for room 1', () => {
        let userList = tempUsers.getUserList('room 1');
        expect(userList).toEqual(['user 1','user 2'])
    })

    it('should return names for room 2', () => {
        let userList = tempUsers.getUserList('room 2');
        expect(userList).toEqual(['user 3'])
    })

    it('should remove a user', () => {
        let userId = '1'
        let user = tempUsers.removeUser(userId);
        expect(user.id).toBe(userId);
        expect(tempUsers.users.length).toBe(2);
    })

    it('should not remove a user', () => {
        let userId = '99'
        let user = tempUsers.removeUser(userId);
        expect(user).toBeUndefined();
        expect(tempUsers.users.length).toBe(3);
    })

    it ('should find a user', () => {
        let userId = '2'
        let user = tempUsers.getUser(userId);
        expect(user.id).toBe(userId);
    })

    it ('should not find a user', () => {
        let userId = '99'
        let user = tempUsers.getUser(userId);
        expect(user).toBeUndefined();

    })

});