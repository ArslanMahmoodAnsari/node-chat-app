let expect = require('expect');
let {generateMessage} = require('./messages');

describe('generateMessage', () => {
    it('should generate correct message object', ()=>{
      let from = 'jen';
      let text = 'someMessage';
      let message = generateMessage(from,text);
      
      expect(typeof message.createdAt).toBe('number');
      expect(message).toMatchObject({from, text});
    })
})