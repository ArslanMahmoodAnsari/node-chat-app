// [{
//     id:'',      //for storing the socket id.
//     name:'',    //for storing the user name.
//     room:''     //for storing the room user joined.
// }]

// creating a ES6 class
class Users{
    constructor(){
        this.users = [];
    }

    // Add User to a Room
    addUser(id,name,room){
        let user = {id,name,room};
        this.users.push(user);
        return user;
    }

    // Remove User from a Room
    removeUser(id){
        let user = this.getUser(id);
        if(user){
            this.users = this.users.filter((u) =>{
               return u.id !== id;
            })          
        }
        return user;
    }

    // Get a specific User
    getUser(id){
        let user = this.users.filter((u) => {
            return u.id === id
        })
        return user[0];
    }
    
    // Get all User in the specific Room
    getUserList(room){
        let users = this.users.filter((user) =>{
            return user.room === room;
        })
        let userNames = users.map((user) => {
            return user.name;
        })
        return userNames;
    }
}

module.exports = {Users}