const path = require("path");
const http = require("http");
const express = require("express");
const socketIO = require("socket.io");

const {generateMessage} = require("./utils/messages");
const {isRealString} = require("./utils/validation");
const {Users} = require("./utils/users");
const publicPath = path.join(__dirname,'../public');
const port = process.env.PORT || 3000;
let app = express();
let server = http.createServer(app);
let io = socketIO(server);
let users = new Users();

//Routes
app.use(express.static(publicPath));

io.on('connection',(socket)=>{
    console.log(`new user connected`);

    socket.on('join', (params,callback)=>{
        if(!isRealString(params.name) || !isRealString(params.room)){
          return callback("name and room name are required")
        }
       
        socket.join(params.room);
        users.removeUser(socket.id);//to make sure the new socket.id is the unique id.
        users.addUser(socket.id,params.name,params.room);
        
        io.to(params.room).emit('updateUserList', users.getUserList(params.room));
        socket.emit('newMessage', generateMessage('Admin', 'welcome to chat app'));
        socket.broadcast.to(params.room).emit('newMessage', generateMessage('Admin', `${params.name} has joined.`));

        callback();
    })

    socket.on('disconnect', () => {
        let user = users.removeUser(socket.id);
        if(user){
            io.to(user.room).emit('updateUserList',users.getUserList(user.room));
            io.to(user.room).emit('newMessage',generateMessage('Admin',`${user.name} has left the room.`));
        }
    })
    
    socket.on('createMessage', (message, callback) => {
        let user = users.getUser(socket.id);
        if(user && isRealString(message.text)){
            io.to(user.room).emit('newMessage',generateMessage(user.name, message.text));
        }
        callback();
    })
})

//running the server
server.listen(port, () => {
    console.log(`server started on PORT => ${port}`);
})