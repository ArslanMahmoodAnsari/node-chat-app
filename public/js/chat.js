var socket = io();

// function for auto scrolling down to the latest message
function scrollToBottom(){
    // Selectors
    var messages = jQuery('#messages');
    // for selecting the last message in the list use ".children()" Method 
    var newMessage = messages.children('li:last-child');
    // Heights
    // ".prop" is a Method, Which give us a cross-browser way to fetch a property.
    // We use it instead of jQuery, to make sure that it works on all browsers. 
    var clientHeight = messages.prop('clientHeight');
    var scrollTop = messages.prop('scrollTop');
    var scrollHeight = messages.prop('scrollHeight');
    // for getting the height of the last message
    var newMessageHeight = newMessage.innerHeight();
    // for getting the height of the 2nd last message
    var lastMessageHeight = newMessage.prev().innerHeight();

    if(clientHeight + scrollTop + newMessageHeight +lastMessageHeight >= scrollHeight){
        messages.scrollTop(scrollHeight);
    }
}

socket.on('connect',function(){
    var params = jQuery.deparam(window.location.search);

    socket.emit('join', params, function (err) {
        if(err){
            alert(err);
            window.location.href = '/';
        }
        else{
            console.log("no error");
        }
    });
})

socket.on('disconnect',function(){
    console.log('Disconnected from server');  
})

socket.on('updateUserList', function(users){
    var ol = jQuery('<ol></ol>');
    users.forEach(function(user) {
        ol.append(jQuery('<li></li>').text(user));
    })
    jQuery('#users').html(ol);
});

socket.on('newMessage',function(message){
    var formatedTime = moment(message.createdAt).format('h:mm a');
    var template = jQuery('#message-template').html();
    var html = Mustache.render(template,{
        text:message.text,
        from:message.from,
        createdAt:formatedTime
    });
    jQuery('#messages').append(html); 
    scrollToBottom();
})


// using jQuery to select an element from the html file have id "message-form"
jQuery('#message-form').on('submit',function(e){
    e.preventDefault();
    var messageTextbox = jQuery('[name=message]');

    socket.emit('createMessage',{
        text : messageTextbox.val(),
    }, function(data){
        messageTextbox.val('');
    })
})